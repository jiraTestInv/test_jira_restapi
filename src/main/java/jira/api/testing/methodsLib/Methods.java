package jira.api.testing.methodsLib;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.SearchRestClient;

import jira.api.testing.basics.JiraAccount;

public abstract class Methods {
	JiraRestClient jiraRestClient;
	IssueRestClient issueRestClient;
	ProjectRestClient projectRestclient;
	SearchRestClient searchRestClient;
	
	public Methods(JiraAccount ac) {
		this.jiraRestClient = ac.getJiraRestClient();
		this.issueRestClient = this.jiraRestClient.getIssueClient();
		this.projectRestclient = this.jiraRestClient.getProjectClient();
		this.searchRestClient = this.jiraRestClient.getSearchClient();
	}
	
}
