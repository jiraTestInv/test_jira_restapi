package jira.api.testing.methodsLib;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;

import jira.api.testing.basics.JiraAccount;

public class WriteMethods extends Methods{
	
	public WriteMethods(JiraAccount ac) {
		super(ac);
		// TODO Auto-generated constructor stub
	}

	public void writeCommentToIssue(String jiraIssueKey, String comment) {
		Issue issue = null;
		URI issueURI = null;
		Comment jiraComment = null;
		try {
			issue = (Issue) this.issueRestClient.getIssue(jiraIssueKey).get();
			issueURI = new URI(issue.getSelf().toString() + "/comment/");
			jiraComment = Comment.valueOf(comment);
			this.issueRestClient.addComment(issueURI, jiraComment).claim();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	public void updateIssueDescription(String issueKey, String newDescription) {
	    IssueInput input = new IssueInputBuilder()
	      .setDescription(newDescription)
	      .build();
	    this.issueRestClient.updateIssue(issueKey, input)
	      .claim();
	}
}
