package jira.api.testing.methodsLib;

import com.atlassian.jira.rest.client.api.IssueRestClient;

import jira.api.testing.basics.JiraAccount;

public class DeleteMethods extends Methods{
	public DeleteMethods(JiraAccount ac) {
		super(ac);
		// TODO Auto-generated constructor stub
	}

	public void deleteIssue(IssueRestClient issueClient, String issueKey, boolean deleteSubtasks) {
	    issueClient
	      .deleteIssue(issueKey, deleteSubtasks)
	      .claim();
	}
}
