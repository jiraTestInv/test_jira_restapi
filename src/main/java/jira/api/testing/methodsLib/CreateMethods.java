package jira.api.testing.methodsLib;

import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;

import jira.api.testing.basics.JiraAccount;

public class CreateMethods extends Methods {

	public CreateMethods(JiraAccount ac) {
		super(ac);
		// TODO Auto-generated constructor stub
	}
	
	
//	issueType: 10000 = Epic
//	issueType: 10001 = Story
//	issueType: 10002 = Task
//	issueType: 10003 = Sub-Task
//	issueType: 10004 = Bug
	public String createIssueToProject(String projectKey, long issueType, String issueSummary) {
	    IssueInput newIssue = new IssueInputBuilder(
	      projectKey, issueType, issueSummary).build();
	    return this.issueRestClient.createIssue(newIssue).claim().getKey();
	}
	
	

}
