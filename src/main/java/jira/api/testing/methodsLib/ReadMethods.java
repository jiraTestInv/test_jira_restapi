package jira.api.testing.methodsLib;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.SearchRestClient;
import com.atlassian.jira.rest.client.api.domain.BasicProject;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.Project;
import com.atlassian.jira.rest.client.api.domain.SearchResult;

import jira.api.testing.basics.JiraAccount;

public class ReadMethods extends Methods{
	private String entryLine;
	
	public ReadMethods(JiraAccount ac) {
		super(ac);
		// TODO Auto-generated constructor stub
	}

	public Iterable<Comment> readAllCommentsByIssueKey(String issueKey) {
		Issue issue;
		try {
			issue = this.issueRestClient.getIssue(issueKey).get();
			Iterable<Comment> comments = issue.getComments();
			comments.forEach(c -> {
				entryLine = 
						c.getId().toString() +"\t"
								+ c.getBody() + "\n"
								+ c.getAuthor().toString() + "\n" 
								+ c.getSelf().toString();
				System.out.println(entryLine);
			});
			return comments;
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public Iterable<BasicProject> readAllProjects(){
		Iterable<BasicProject> projects;
		try {
			projects = this.projectRestclient.getAllProjects().get();
			projects.forEach(proj -> {
				entryLine =
						proj.getKey() + "\t"
						+ proj.getName() + "\t"
						+ proj.getId().toString() + "\n"
						+ proj.getSelf().toString();
				System.out.println(entryLine);
			});
			return projects;
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public ArrayList<Issue> readIssuesByKeys(List<String> keys){
		ArrayList<Issue> issues = new ArrayList<Issue>();
		try {
			keys.forEach(k -> {
				Issue is;
				try {
					is = (Issue) this.issueRestClient.getIssue((String) k).get();
					System.out.println(is.getKey());
					issues.add(is);
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			return issues;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public Iterable<Issue> readIssuesByProject(String project){
		String jql = "project=" + project;
		try {
			SearchResult results = this.searchRestClient.searchJql(jql).get();
			results.getIssues().forEach(i -> {
				System.out.printf("%s; %s; %s; %s;%n", i.getKey(), i.getComments(), i.getCreationDate(), i.getDescription());
			});
			return results.getIssues();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}
	
	public Iterable<Issue> readIssuesByJql(String jql){
		try {
			SearchResult results = this.searchRestClient.searchJql(jql).get();
			return results.getIssues();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}
	
}
