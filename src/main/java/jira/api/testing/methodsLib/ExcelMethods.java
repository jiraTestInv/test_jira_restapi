package jira.api.testing.methodsLib;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jira.api.testing.basics.JiraAccount;

public class ExcelMethods extends Methods{
	private String fileLocation;
	private FileInputStream inputStream;
	private Workbook workbook;
	private Sheet worksheet;
	private Map<Integer, List<String>> data;
	
	public ExcelMethods(JiraAccount ac, String loc) {
		super(ac);
		this.fileLocation = loc;
		try {
			this.inputStream = new FileInputStream(new File(this.fileLocation));
			this.workbook = new XSSFWorkbook(this.inputStream);
			this.worksheet = this.workbook.getSheetAt(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void checkCellDataType(){
		Map<Integer, List<String>> data = new HashMap<>();
		int i = 0;
		for (Row row : this.worksheet) {
		    data.put(i, new ArrayList<String>());
		    for (Cell cell : row) {
		        switch (cell.getCellType()) {
		            case STRING: System.out.println(cell.getCellType().toString()); break;
		            case NUMERIC: System.out.println(cell.getCellType().toString()); break;
		            case BOOLEAN: System.out.println(cell.getCellType().toString()); break;
		            case FORMULA: System.out.println(cell.getCellType().toString()); break;
		            default: data.get(new Integer(i)).add(" ");
		        }
		    }
		    i++;
		}
	}
	
}
