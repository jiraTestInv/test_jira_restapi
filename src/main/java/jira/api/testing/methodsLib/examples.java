package jira.api.testing.methodsLib;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.domain.Attachment;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;

public class examples {
	
	public void addCommentJIRA(IssueRestClient issueRestClient, String jiraIssueKey, String comment) {
		Issue issue = null;
		URI issueURI = null;
		Comment jiraComment = null;
		try {
			issue = (Issue) issueRestClient.getIssue(jiraIssueKey).get();
			issueURI = new URI(issue.getSelf().toString() + "/comment/");
			jiraComment = Comment.valueOf(comment);
			issueRestClient.addComment(issueURI, jiraComment).claim();
		} catch (InterruptedException e) {
		} catch (ExecutionException e) {
		} catch (URISyntaxException e) {

		}

	}

	public void attachLogsJiraAsStream(ByteArrayInputStream bin, String fileName, String jiraIssueKey,
			IssueRestClient issueRestClient) {

		// Calls the getIssue Client and adds Attachment

		Issue issue = (Issue) issueRestClient.getIssue(jiraIssueKey).claim();

		issueRestClient.addAttachment(issue.getAttachmentsUri(), bin, fileName);

	}


	public ByteArrayOutputStream downloadAttachmentsAsStream(IssueRestClient issueRestClient, String jiraAttachmentUri,
			String attachmentName) {
		String jiraAttachmentUr="http://www.example.com/jira/attachments/10000";
		InputStream in = null;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		try {

			in = (InputStream) issueRestClient.getAttachment(new URI(jiraAttachmentUri)).get();
			byte[] buffer = new byte[49152];
			int bytesRead;
			while ((bytesRead = in.read(buffer)) != -1) {
				bout.write(buffer, 0, bytesRead);
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		} catch (ExecutionException e) {

			e.printStackTrace();
		} catch (URISyntaxException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		return bout;
	}

	public ArrayList<String> getJiraAttachmentName(IssueRestClient issueRestClient, String jiraIssueKey) {
		ArrayList<String> jiraAttachmentNameList = new ArrayList<String>();
		Issue issue = (Issue) issueRestClient.getIssue(jiraIssueKey).claim();
		Iterable<Attachment> issueAttachments = issue.getAttachments();
		issueAttachments.forEach(attachment -> {
			if (null != attachment.getFilename()) {
				jiraAttachmentNameList.add(attachment.getFilename());
			}
		});
		return jiraAttachmentNameList;
	}

	/*
	 * Gets the attachment Url List
	 */

	public ArrayList<String> getJiraAttachmentsURLList(IssueRestClient issueRestClient, String jiraIssueKey) {

		ArrayList<String> jiraAttachmentURLList = new ArrayList<String>();
		Issue issue = (Issue) issueRestClient.getIssue(jiraIssueKey).claim();
		Iterable<Attachment> issueAttachments = issue.getAttachments();
		issueAttachments.forEach(attachment -> {
			if (null != attachment.getFilename()) {
				jiraAttachmentURLList.add((attachment.getContentUri().toString()));
			}
		});
		return jiraAttachmentURLList;
	}
}
