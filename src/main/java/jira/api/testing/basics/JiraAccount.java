package jira.api.testing.basics;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.StringJoiner;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;

public class JiraAccount {
	private String username;
	private String password;
	private URI serverURI;
	
//	constructor (basic)
	public JiraAccount() {
	}
	
//	constructor (easy version to set properties)
	public JiraAccount(String user, String pw, String server) {
		this.username = user;
		this.password = pw;
		try {
			this.serverURI = new URI(server);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	get a jiraRestClient object using its own properties
	public JiraRestClient getJiraRestClient() {
		JiraRestClient jiraRestClient = null;
		try {
			JiraRestClientFactory restClientFactory = new AsynchronousJiraRestClientFactory();
			jiraRestClient = restClientFactory.createWithBasicHttpAuthentication(this.serverURI, this.username, this.password);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return jiraRestClient;
	}
	
//	get a jiraRestClient object using another account's properties
	public JiraRestClient getJiraRestClient(JiraAccount account) {
		JiraRestClient jiraRestClient = null;
		try {
			JiraRestClientFactory restClientFactory = new AsynchronousJiraRestClientFactory();
			jiraRestClient = restClientFactory.createWithBasicHttpAuthentication(account.serverURI, account.username, account.password);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return jiraRestClient;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public URI getServerURI() {
		return serverURI;
	}
	public void setServerURI(URI serverURI) {
		this.serverURI = serverURI;
	}
	
	public String toString() {
		StringJoiner output = new StringJoiner("\n");
		output.add(this.username).add(this.serverURI.toString());
		return output.toString();
	}
}
