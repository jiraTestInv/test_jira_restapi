package jira.api.testing.testREST;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		Scanner mySc = new Scanner(System.in);
		int i = 0;
		do {
			try {
				i = mySc.nextInt();
				System.out.printf("Input was: %d%n", i);
				
			} catch (InputMismatchException e) {
				System.out.println(e.getMessage());
				// TODO: handle exception
			}
		} while (i != 0);
	}
}
