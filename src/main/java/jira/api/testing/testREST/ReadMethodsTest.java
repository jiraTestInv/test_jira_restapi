package jira.api.testing.testREST;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.atlassian.jira.rest.client.api.domain.BasicProject;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;

import jira.api.testing.basics.JiraAccount;
import jira.api.testing.methodsLib.ReadMethods;
import jira.api.testing.methodsLib.WriteMethods;

public class ReadMethodsTest {
	final static String SERVER_URI = "https://jiratest3.atlassian.net";
	final static String USER_NAME = "jira.test3.inv@gmail.com";
	final static String PASSWORD = "1nvensity";
//	final static String SERVER_URI = "https://rb-tracker.bosch.com/tracker08/secure/RapidBoard.jspa?rapidView=3687&view=detail&selectedIssue=PJPH-21836";
//	final static String USER_NAME = "hos1bb";
//	final static String PASSWORD = "LOV4600edeu";
	final static String ISSUE_KEY = "T1-1";
	final static String COMMENT = "This comment was added by Java-API";

	public static void main(String[] args) {

		JiraAccount myAccount = new JiraAccount(USER_NAME, PASSWORD, SERVER_URI);
		System.out.println(myAccount.toString());
		ReadMethods myReadMethods = new ReadMethods(myAccount);
		
		ArrayList<BasicProject> projects = (ArrayList<BasicProject>) myReadMethods.readAllProjects();
//		
//		ArrayList<Issue> issues = (ArrayList<Issue>) myReadMethods.readIssuesByProject(projects.get(0).getName().toString());
//		List<String> keyList = Arrays.asList("T1-1", "T1-2");
//				
//		ArrayList<Issue> issues2 = myReadMethods.readIssuesByKeys(keyList);
//		Iterable<Comment> comment = myReadMethods.readAllCommentsByIssueKey(issues2.get(0).getKey());
////		myReadMethods.readIssuesByJql("");
//		
//		WriteMethods myWriteMethods = new WriteMethods(myAccount);
//		myWriteMethods.writeCommentToIssue(issues2.get(1).getKey(), "This a new comment added by the writeMethod in Java， Vincent!");
//		
//		myReadMethods.readAllCommentsByIssueKey(issues2.get(1).getKey());
//		Iterable<BasicProject> myProjects = myReadMethods.readAllProjects();
//
//		String keyNewIssue = myWriteMethods.createIssue("SNDB", (long) 10002, "Issue-3 per Java-API");
//		myWriteMethods.updateIssueDescription(keyNewIssue, "I added this description per Java-API");
//		
//		Issue myIssue = (Issue)myAccount.getJiraRestClient().getIssueClient()
//				.getIssue(ISSUE_KEY)
//				.claim();
//		
//		System.out.println(myIssue.getAssignee().getName() + ":: " + myIssue.getAssignee().getEmailAddress());
//
//		Iterable<Comment> myComments = myReadMethods.readAllComments(myIssue);
//
//		try {
//			System.out.println("name of project: " + myAccount.getJiraRestClient().getProjectClient().getProject("SNDB").get().getName());
//			System.out.println("key of project: " + myAccount.getJiraRestClient().getProjectClient().getProject("SNDB").get().getKey());
//		} catch (InterruptedException | ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		System.out.println("End of program.");
	}
}