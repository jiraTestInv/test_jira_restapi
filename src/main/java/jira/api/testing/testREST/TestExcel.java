package jira.api.testing.testREST;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestExcel {

	public static void main(String[] args) {
		String fileLocation = "C:\\Users\\shichen.hong\\Documents\\Arbeit\\jiraTestFiles\\test_java.xlsx";
		try {
			FileInputStream file = new FileInputStream(new File(fileLocation));
			Workbook myWb = new XSSFWorkbook(file);
			Sheet mySh = myWb.getSheetAt(0);
			
			Map<Integer, List<String>> data = new HashMap<>();
			int i = 0;
			for (Row row : mySh) {
			    data.put(i, new ArrayList<String>());
			    for (Cell cell : row) {
			        switch (cell.getCellTypeEnum()) {
			            case STRING: 
			            	data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
			            	System.out.println(cell.getAddress() + ": String"); break;
			            case NUMERIC: 
			            	if (DateUtil.isCellDateFormatted(cell)) {
			            	    data.get(i).add(cell.getDateCellValue() + "");
			            	} else {
			            	    data.get(i).add(cell.getNumericCellValue() + "");
			            	}
			            	System.out.println(cell.getAddress() + ": Numeric"); break;
			            case BOOLEAN: 
			            	data.get(i).add(cell.getBooleanCellValue() + "");
			            	System.out.println(cell.getAddress() + ": Boolean"); break;
			            case FORMULA: 
			            	data.get(i).add(cell.getCellFormula() + "");
			            	System.out.println(cell.getAddress() + ": Formula"); break;
			            default: data.get(new Integer(i)).add(" ");
			        }
			    }
			    i++;
			}
			System.out.println(data.size());
		}catch(
	IOException e)
	{
		e.printStackTrace();
	}
}}
